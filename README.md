# Benevolaide Project

![preview](public/0cfb9800-cf27-11e9-82da-48d384516d51.png)

## Benevolaide Mockups

Mockups of the Benevolaide website. It focus on visual, ergonomics and usability, and structurate the website with examples of specific webpages. This project only contains client-side code like HTML, CSS and a drop of Javascript. 

## What is Benevolaide?

Benevolaide is a volunteer and beneficiary management website. It aims to help the team of the Secours Populaire Français to keep tracks of its beneficiaries while they help them search for job, create Curriculum Vitae and Applications Letters. It keep tracks of volunteer actions and meetings.  
